#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QHostInfo>
#include <QFile>
#include <QTextEdit>
#include <QSettings>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  QTextEdit *outTextEdit;
  QProcess *proc;

  QString getLocalIP();

private slots:

  void on_buttonBox_accepted();

  void on_act_confDomainController_triggered();

  bool setParam(QString fileName, QString paramName, QString paramValue, QString delimiter = "=");


  void on_lineEdit_domainName_editingFinished();

  void on_action_2_triggered();

  void on_action_6_triggered();

  void on_action_8_triggered();

  void readProcOutput();

  void on_buttonBox_2_accepted();

  void saveConfig(QString fileName);
  void loadConfig(QString fileName);



  void on_action_save_triggered();

  void on_action_load_triggered();

private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
