#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QHostInfo>
#include <QFileDialog>


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
//  connect(ui->act_confDomainController,SIGNAL(triggered()),this,SLOT(on_act_confDomainController_triggered()));
  outTextEdit = new QTextEdit(this);
  QPalette p = outTextEdit->palette();
  p.setColor(QPalette::Base,QColor(0,0,0));
  p.setColor(QPalette::Text,QColor(240,240,240));
  outTextEdit->setPalette(p);
  ui->centralWidget->layout()->addWidget(outTextEdit);
  outTextEdit->hide();
  ui->widgetServerConf->hide();
  ui->widgetClientConf->hide();

  proc = new QProcess(this);
  proc->setReadChannel(QProcess::StandardOutput);
//  proc->setStandardOutputFile("/etc/ald/ald-admin.log");
  connect(proc,SIGNAL(readyReadStandardOutput()),this,SLOT(readProcOutput()));
  connect(proc,SIGNAL(readyReadStandardError()) ,this,SLOT(readProcOutput()));
  connect(ui->lineEdit_domainName_2,SIGNAL(editingFinished()),this,SLOT(on_lineEdit_domainName_editingFinished()));
}

MainWindow::~MainWindow()
{
  delete ui;
  if (outTextEdit)
        delete outTextEdit;
}



void MainWindow::on_buttonBox_accepted()
{
    QStringList ErrMsg;
    if (ui->lineEdit_serverName->text().isEmpty()) {
        ErrMsg.append(trUtf8("Имя сервера не должно быть пустым."));
      }
    if (ui->lineEdit_domainName->text().isEmpty()) {
        ErrMsg.append( trUtf8("Имя домена не должно быть пустым."));
      }
    if (ui->lineEdit_kerberosPassword->text().isEmpty()) {
        ErrMsg.append(trUtf8("Пароль Kerberos не может быть пустым."));
      }
    if (ui->lineEdit_ALDPassword->text().isEmpty()) {
        ErrMsg.append(trUtf8("Пароль ALD не может быть пустым."));
      }
    if (QString(".")+ui->lineEdit_ALDPassword->text()==ui->lineEdit_domainName->text()) {
        ErrMsg.append(trUtf8("Пароль к БД ALD не должен совпадать с именем домена."));
      }
    if (QString(".")+ui->lineEdit_kerberosPassword->text()==ui->lineEdit_domainName->text()) {
        ErrMsg.append(trUtf8("Пароль к БД Kerberos не должен совпадать с именем домена."));
      }
    if (!ErrMsg.isEmpty()) {
         QMessageBox msg;
         foreach(QString errMsg,ErrMsg) {
             msg.setText(errMsg);
             msg.exec();
           }
         return;
      }
     outTextEdit->clear();
     outTextEdit->show();
//     ui->widgetServerConf->setEnabled(false);
     qApp->processEvents();
     // Сменить имя хоста на новое
     proc->start(QString("hostname"),QStringList() << ui->lineEdit_serverName->text());
     // Добавить в /etc/hosts запись для хоста
     setParam(QString("/etc/hosts"),ui->lineEdit_IP->text(),ui->lineEdit_serverName->text(),QString(" "));

     // Добавляем параметры в ald.conf
     setParam(QString("/etc/ald/ald.conf"),"DOMAIN",ui->lineEdit_domainName->text());
     setParam(QString("/etc/ald/ald.conf"),"SERVER",ui->lineEdit_serverName->text());

     QByteArray str1;
     str1.append("K/M:"+ui->lineEdit_kerberosPassword->text()+"\n");
     QString str2("admin/admin:");
     str2+=ui->lineEdit_ALDPassword->text();
     str2+="\n";

     QString passwdFileName("/etc/ald/passwd");
     setParam(passwdFileName,"K/M",ui->lineEdit_kerberosPassword->text(),":");
     setParam(passwdFileName,"admin/admin",ui->lineEdit_ALDPassword->text(),":");
     QFile passwdFile(passwdFileName);
     if (!passwdFile.setPermissions(QFile::ReadOwner|QFile::WriteOwner))
          qDebug() << "Can not set permissions";
     QMessageBox msg;
     msg.setText(trUtf8("Если на данной стадии наблюдается зависание, то, возможно, генератору случайных чисел на сервере не хватает данных. Подвигайте мышкой, чтобы сгенерировать случайные данные."));
     msg.exec();

     proc->start("ald-init stop -f");
     while (!proc->waitForFinished(100))
         qApp->processEvents();
     proc->start("ald-init init -f --pass-file=/etc/ald/passwd");
     while (!proc->waitForFinished(100))
         qApp->processEvents();
     if ((proc->exitStatus() == 0) && (proc->exitCode() == 0))
     {
        msg.setText(trUtf8("ALD сервер успешно инициализирован"));
        msg.exec();
     } else {
         msg.setText(trUtf8("Произошла ошибка инициализации ALD сервера"));
         msg.exec();
     }
}


void MainWindow::on_buttonBox_2_accepted()
{
    QStringList ErrMsg;
    if (ui->lineEdit_serverName_2->text().isEmpty()) {
        ErrMsg.append(trUtf8("Имя сервера не должно быть пустым."));
      }
    if (ui->lineEdit_domainName_2->text().isEmpty()) {
        ErrMsg.append( trUtf8("Имя домена не должно быть пустым."));
      }
    if (ui->lineEdit_IP_2->text().isEmpty()) {
        ErrMsg.append(trUtf8("IP-адрес сервера не может быть пустым."));
      }
    if (ui->lineEdit_ALDPassword_2->text().isEmpty()) {
        ErrMsg.append(trUtf8("Пароль ALD не может быть пустым."));
      }
    if (ui->lineEdit_ClientIP->text().isEmpty()) {
        ErrMsg.append(trUtf8("IP-адрес клиента не может быть пустым."));
      }
    if (ui->lineEdit_ClientName->text().isEmpty()) {
        ErrMsg.append(trUtf8("Имя клиента не может быть пустым."));
      }

    if (!ErrMsg.isEmpty()) {
         QMessageBox msg;
         foreach(QString errMsg,ErrMsg) {
             msg.setText(errMsg);
             msg.exec();
           }
         return;
      }

     outTextEdit->clear();
     outTextEdit->show();
     setParam(QString("/etc/hosts"),ui->lineEdit_ClientIP->text(),ui->lineEdit_ClientName->text()," ");
     QStringList Script;
     Script << "#!/bin/bash";
     Script << "set -e";
     Script << QString(trUtf8("echo Устанавливаем имя клиента hostname %1")).arg(ui->lineEdit_ClientName->text());

     Script << QString("hostname %1").arg(ui->lineEdit_ClientName->text());

//Добавление ip-адреса и имени сервера
     Script << QString(trUtf8("echo Добавляем в hosts ip и имя сервера %1 %2")).arg(ui->lineEdit_IP->text()).arg(ui->lineEdit_serverName->text());
     Script << QString("if grep %1 /etc/hosts; then").arg(ui->lineEdit_IP_2->text());
     Script << QString("  sed -i \"s/%1 .*/%1 %2/\" /etc/hosts ").arg(ui->lineEdit_IP_2->text()).arg(ui->lineEdit_serverName->text());
     Script << QString("else ");
     Script << QString("   echo %1 %2 >> /etc/hosts").arg(ui->lineEdit_IP_2->text()).arg(ui->lineEdit_serverName_2->text());
     Script << "fi";

//Добавление ip-адреса и имени компьютера
     Script << QString(trUtf8("echo Добавляем в hosts ip и имя клиента %1 %2")).arg(ui->lineEdit_ClientIP->text()).arg(ui->lineEdit_ClientName->text());
     Script << QString("if grep %1 /etc/hosts; then").arg(ui->lineEdit_ClientIP->text());
     Script << QString("  sed -i \"s/%1 .*/%1 %2/\" /etc/hosts ").arg(ui->lineEdit_ClientIP->text()).arg(ui->lineEdit_ClientName->text());
     Script << QString("else ");
     Script << QString("   echo %1 %2 >> /etc/hosts").arg(ui->lineEdit_ClientIP->text()).arg(ui->lineEdit_ClientName->text());
     Script << "fi";

//Изменение имени домена
     Script << trUtf8("echo настройка ald.conf");
     Script << QString(trUtf8("echo Добавляем в ald.conf параметр DOMAIN %1 ")).arg(ui->lineEdit_domainName_2->text());
     Script << QString("if grep DOMAIN= /etc/ald/ald.conf; then");
     Script << QString("  sed -i \"s/DOMAIN=.*/DOMAIN=\\")+ui->lineEdit_domainName_2->text()+"/\" /etc/ald/ald.conf ";
     Script << QString("else ");
     Script << QString("   echo DOMAIN=%1 >> /etc/ald.conf").arg(ui->lineEdit_domainName_2->text());
     Script << "fi";


//Изменение имени сервера
     Script << QString(trUtf8("echo Добавляем в ald.conf параметр SERVER %1 ")).arg(ui->lineEdit_serverName_2->text());
     Script << QString("if grep SERVER= /etc/ald/ald.conf; then");
     Script << QString("  sed -i \"s/SERVER=.*/SERVER=")+ui->lineEdit_serverName_2->text()+"/\" /etc/ald/ald.conf ";
     Script << QString("else ");
     Script << QString("   echo SERVER=%1 >> ald.conf").arg(ui->lineEdit_serverName_2->text());
     Script << "fi";

//Выполнение ald-client
     Script << trUtf8("echo Создаем файл с паролем");
     Script << QString("echo \"admin/admin:%1\" > /etc/ald/passwd").arg(ui->lineEdit_ALDPassword_2->text());
     Script << QString("chmod 600 /etc/ald/passwd");


     Script << trUtf8("echo Запускаем ald-client start");
     Script << QString("ald-client start -f --pass-file=/etc/ald/passwd");
     Script << trUtf8("echo Запускаем ald-client commit-config");
     Script << QString("ald-client commit-config -f --pass-file=/etc/ald/passwd");

     Script << QString("rm /etc/ald/passwd");

     if (false) {

//               if grep 192.168.31.249 /etc/hosts; then
//                 echo param 192.168.31.249 found
//                 sed -i "s/192.168.31.249 .*/192.168.31.249 client1/" /etc/hosts
//               else
//                  echo 192.168.31.249 client1 >> /etc/hosts
//               fi

//               if grep SERVER /etc/ald/ald.conf; then
//                 echo param SERVER found
//                 sed -i "s/SERVER=.*/SERVER=qqq/" /etc/ald/ald.conf
//               else
//                  echo SERVER=qqq >> /etc/ald/ald.conf
//               fi

//               if grep DOMAIN /etc/ald/ald.conf; then
//                 echo param DOMAIN found
//                 sed -i "s/DOMAIN=.*/DOMAIN=.www/" /etc/ald/ald.conf
//               else
//                  echo DOMAIN=.www >> /etc/ald/ald.conf
//               fi



//               echo "admin/admin:qqq" > /etc/ald/passwd
//               chmod 600 /etc/ald/passwd

//               ald-client start -f --pass-file=/etc/ald/passwd
//               ald-client commit-config -f --pass-file=/etc/ald/passwd --update-keytab"
//     QString Script;
//     Script += QString(" hostname ")+ui->lineEdit_ClientName->text()+QString("\n");
//     Script += QString(" if grep %2 %1; then "
//                       "     echo param %2 found "
//                       "     sed -i \"s/%2 .*/%2 %3/\" $1 "
//                       " else "
//                       "   echo %2 %3 >> %1 "
//                       " fi \n").arg("/etc/hosts").arg(ui->lineEdit_IP->text()).
//                        arg(ui->lineEdit_serverName->text());
     }
     QFile scriptFile("/root/remoteScript.sh");
     scriptFile.setPermissions(QFile::ExeOwner|QFile::ReadOwner|QFile::WriteOwner);
     scriptFile.open(QIODevice::WriteOnly|QIODevice::Text);
     QTextStream stream(&scriptFile);
     foreach( QString line, Script) {
         stream << line << endl;
     }
     scriptFile.close();
     qApp->processEvents();



//Создание startScript

     QStringList startScript;
     startScript << "#!/bin/bash";
     startScript << QString("ssh root@%1 ' bash -s ' < /root/remoteScript.sh").arg(ui->lineEdit_ClientIP->text());

     QFile startScriptFile("/root/startScript.sh");
     startScriptFile.setPermissions(QFile::ExeOwner|QFile::ReadOwner|QFile::WriteOwner);
     startScriptFile.open(QIODevice::WriteOnly|QIODevice::Text);
     QTextStream stream1(&startScriptFile);
     foreach( QString line, startScript) {
         stream1 << line << endl;
     }
     startScriptFile.close();
     qApp->processEvents();

     QString cmd;
     cmd = "/root/startScript.sh" ;
     proc->start(cmd);
     while (!proc->waitForFinished(100)) {
         qApp->processEvents();
     }

// TODO: uncomment 2 lines below to remove scripts
//     startScriptFile.remove();
//     scriptFile.remove();
     QMessageBox msg;
     if ((proc->exitStatus() == 0) && (proc->exitCode() == 0))
     {
        msg.setText(trUtf8("Клиент %1 успешно добавлен в домен %2").arg(ui->lineEdit_ClientName->text()).arg(ui->lineEdit_domainName_2->text()));
        msg.exec();
     } else {
         msg.setText(trUtf8("Произошла ошибка добавления клиента %1 в домен %2").arg(ui->lineEdit_ClientName->text()).arg(ui->lineEdit_domainName_2->text()));
         msg.exec();
     }
}


void MainWindow::on_act_confDomainController_triggered()
{
//  QHostInfo info = QHostInfo::fromName(QHostInfo::localHostName());
//  QString hostname = info.localHostName();
//  qDebug() << info.hostName();
//  QList<QHostAddress> adresses = info.addresses();
//    foreach (QHostAddress addr, adresses) {
//        qDebug() << addr.toString();
//      }
   ui->widgetServerConf->show();
   ui->widgetClientConf->hide();
   outTextEdit->hide();

   ui->lineEdit_IP->setText(getLocalIP());
}

bool MainWindow::setParam(QString fileName, QString paramName, QString paramValue, QString delimiter)
{

  QFile file(fileName);
  QFile file_new(fileName+"_");
      if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
           qDebug() << file.errorString();
           return false;
      }
  if (!file_new.open(QIODevice::WriteOnly | QIODevice::Text)) {
       qDebug() << file_new.errorString();
       return false;
  }


  QByteArray line;
  bool paramExists = false;
  while (!file.atEnd()) {
            line = file.readLine();
            QString string(line);
            QRegExp rx(paramName+"[ \t]*"+delimiter);
            if (rx.indexIn(string)!=-1){
                qDebug() << string;
                paramExists = true;
                line.clear();
                line.append(paramName+delimiter+paramValue+"\n");
            }
            file_new.write(line);
        }
  file.close();
  file_new.close();

  if (!paramExists) {
      if (!file_new.open(QIODevice::Append | QIODevice::Text)) {
           qDebug() << file.errorString();
           return false;
      }
      line.clear();
      line.append(paramName+delimiter+paramValue+"\n");
      file_new.write(line);
      file_new.close();
    }
  file.remove();
  file_new.rename(fileName);
  return true;
}

void MainWindow::on_lineEdit_domainName_editingFinished()
{
  QLineEdit *edit = (QLineEdit *)sender();
  QString domain = edit->text();
  if (domain[0] != '.') {
      domain = QString(".")+domain;
      edit->setText(domain);
   }
}

void MainWindow::on_action_2_triggered()
{

  ui->widgetServerConf->hide();
  ui->widgetClientConf->show();
  outTextEdit->hide();

  if (!ui->lineEdit_ALDPassword->text().isEmpty())
      ui->lineEdit_ALDPassword_2->setText(ui->lineEdit_ALDPassword->text());

  if (!ui->lineEdit_domainName->text().isEmpty())
      ui->lineEdit_domainName_2->setText(ui->lineEdit_domainName->text());

  if (!ui->lineEdit_serverName->text().isEmpty())
      ui->lineEdit_serverName_2->setText(ui->lineEdit_serverName->text());

  if (!ui->lineEdit_IP->text().isEmpty())
      ui->lineEdit_IP_2->setText(ui->lineEdit_IP->text());
  else
      ui->lineEdit_IP_2->setText(getLocalIP());



}

void MainWindow::on_action_6_triggered()
{
  QMessageBox msg;
  msg.setText(trUtf8("Программа \"Администратор ALD\" предназначена для первичной настройки сервера домена и удаленного добавления компьютеров в домен."));
  msg.exec();

}

void MainWindow::on_action_8_triggered()
{
  QMessageBox msg;
  msg.setText(trUtf8("ALD-admin 1.0\n\nАвторы:\nНевров А.А.\nПитрук П.Е.\n\nОрёл 2014"));
  msg.exec();

}


void MainWindow::readProcOutput()
{
   QByteArray *out = new QByteArray;
   *out = proc->readAllStandardOutput();
   outTextEdit->insertPlainText(trUtf8(out->data()));
   *out = proc->readAllStandardError();
   outTextEdit->insertPlainText(trUtf8(out->data()));
   QTextCursor c = outTextEdit->textCursor();
   c.movePosition(QTextCursor::End);
   outTextEdit->setTextCursor(c);

}



QString MainWindow::getLocalIP()
{
    QProcess pr;
    pr.start("ifconfig");
    pr.waitForFinished();
    QString out(pr.readAll());
    QRegExp regEx("inet addr:(\\d{1,}\\.\\d{1,}\\.\\d{1,}\\.\\d{1,})");
    QStringList list;
    int pos = 0;
    while ((pos = regEx.indexIn(out, pos)) != -1) {
        list << regEx.cap(1);
        pos += regEx.matchedLength();
    }
//    qDebug() << out;
//    qDebug() << list;
    return list[0];
}


void MainWindow::saveConfig(QString fileName)
{
    QSettings *settings = new QSettings(fileName,QSettings::IniFormat);
    settings->setValue("common/ServerName",ui->lineEdit_serverName->text());
    settings->setValue("common/ServerIP",ui->lineEdit_IP->text());
    settings->setValue("common/DomainName",ui->lineEdit_domainName->text());
    settings->setValue("common/ALDPassword",ui->lineEdit_ALDPassword->text());

    settings->setValue("server/KerberosPassword",ui->lineEdit_kerberosPassword->text());

    settings->setValue("client/ClientName",ui->lineEdit_ClientName->text());
    settings->setValue("client/ClientIP",ui->lineEdit_ClientIP->text());

    settings->sync();
    QFile file(fileName);
    file.setPermissions(QFile::ReadOwner|QFile::WriteOwner);
}


void MainWindow::loadConfig(QString fileName)
{
    QSettings *settings = new QSettings(fileName,QSettings::IniFormat);
    ui->lineEdit_ALDPassword->setText(settings->value("common/ALDPassword","").toString());
    ui->lineEdit_ALDPassword_2->setText(settings->value("common/ALDPassword","").toString());

    ui->lineEdit_ClientIP->setText(settings->value("client/ClientIP","").toString());
    ui->lineEdit_ClientName->setText(settings->value("client/ClientName","").toString());

    ui->lineEdit_domainName->setText(settings->value("common/DomainName","").toString());
    ui->lineEdit_domainName_2->setText(settings->value("common/DomainName","").toString());

    ui->lineEdit_IP->setText(settings->value("common/ServerIP","").toString());
    ui->lineEdit_IP_2->setText(settings->value("common/ServerIP","").toString());

    ui->lineEdit_kerberosPassword->setText(settings->value("server/KerberosPassword","").toString());

    ui->lineEdit_serverName->setText(settings->value("common/ServerName","").toString());
    ui->lineEdit_serverName_2->setText(settings->value("common/ServerName","").toString());
}

void MainWindow::on_action_save_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                  QString(trUtf8("Укажите имя файла для сохранения конфигурации")),
                                                  "",
                                                  trUtf8("Файл конфигурации (*.conf)"));
    if (!fileName.isEmpty())
        saveConfig(fileName);
}

void MainWindow::on_action_load_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                  QString(trUtf8("Укажите имя файла для сохранения конфигурации")),
                                                  "",
                                                  trUtf8("Файл конфигурации (*.conf)"));
    if (!fileName.isEmpty())
        loadConfig(fileName);

}
